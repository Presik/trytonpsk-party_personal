# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import party
from . import nationality
from . import preference


def register():
    Pool.register(
        nationality.Nationality,
        preference.Preference,
        party.Party,
        party.PartyPreference,
        party.PartyByCategoryStart,
        module='party_personal', type_='model')
    Pool.register(
        party.PartyByCategory,
        module='party_personal', type_='wizard')
    Pool.register(
        party.PartyByCategoryReport,
        module='party_personal', type_='report')
