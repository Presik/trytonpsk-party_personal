# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class Nationality(ModelSQL, ModelView):
    'Nationality'
    __name__ = 'party.nationality'
    name = fields.Char('Name', required=True)
    code = fields.Integer('Code')
    demonym = fields.Char('Demonym')
    symbol = fields.Char('Symbol')
    visa_out = fields.Selection([
            ('y', 'Y'),
            ('n', 'N'),
            ('', ''),
            ], 'Visa Out')
    visa_in = fields.Selection([
            ('y', 'Y'),
            ('n', 'N'),
            ('', ''),
            ], 'Visa In')
